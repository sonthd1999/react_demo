import React, { useState } from "react";
import {
  TextField,
  Button,
  Grid,
  FormControl,
  InputLabel,
  Select,
  FormControlLabel,
  Checkbox,
  Box,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import { Rating } from "@material-ui/lab";
import { Formik, ErrorMessage, Form } from "formik";
import * as Yup from "yup";

const useStyles = makeStyles((theme) => ({
  main: {
    display: "flex",
    justifyContent: "center",
  },
  container: {
    backgroundColor: "white",
  },
  header: {
    margin: theme.spacing(4, 0, 0),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "300px !important",
    marginTop: theme.spacing(1),
  },
  submit: {
    textAlign: "center",
  },
}));

function FormMaterialUI() {
  const classes = useStyles();

  const [initialValue, setInitialValue] = useState({
    username: "",
    email: "",
    password: "",
    position: "",
    agree: false,
    rating: 0,
    language: [],
  });

  return (
    <Grid container className={classes.main} xs={12}>
      <Formik
        enableReinitialize
        initialValues={initialValue}
        onSubmit={(values, actions) => {
          setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            actions.setSubmitting(false);
          }, 1000);
        }}
        validateOnChange={true}
        validateOnBlur={true}
        validationSchema={Yup.object().shape({
          username: Yup.string().required(),
          email: Yup.string().email().required(),
          password: Yup.string().required(),
          position: Yup.string().required(),
          rating: Yup.string(),
          language: Yup.array(),
        })}
      >
        {(props) => {
          props.submitCount > 0 && (props.validateOnChange = true);
          const {
            values,
            isSubmitting,
            handleBlur,
            handleChange,
            handleSubmit,
            handleReset,
          } = props;

          return (
            <div className={classes.container}>
              <div className={classes.header}>
                <Avatar className={classes.avatar}>
                  <LockOutlinedIcon />
                </Avatar>
                <h2>Formik Login</h2>
              </div>
              <div className={classes.form}>
                <Form onSubmit={handleSubmit}>
                  <Grid>
                    <TextField
                      fullWidth
                      name="username"
                      label="UserName"
                      placeholder="Enter your Name"
                      value={values.username}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    <Grid className="input-feedback text-change">
                      <ErrorMessage name="username" />
                    </Grid>
                  </Grid>
                  <Grid>
                    <TextField
                      fullWidth
                      name="email"
                      label="Email"
                      placeholder="Enter your Email"
                      value={values.email}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    <Grid className="input-feedback text-change">
                      <ErrorMessage name="email" />
                    </Grid>
                  </Grid>
                  <Grid>
                    <TextField
                      fullWidth
                      name="password"
                      label="Password"
                      placeholder="Enter your Password"
                      type="password"
                      value={values.password}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    <Grid className="input-feedback text-change">
                      <ErrorMessage name="password" />
                    </Grid>
                  </Grid>
                  <Grid>
                    <FormControl fullWidth>
                      <InputLabel htmlFor="position">Position</InputLabel>
                      <Select
                        native
                        value={values.position}
                        onChange={handleChange}
                        inputProps={{
                          name: "position",
                          id: "position",
                        }}
                      >
                        <option aria-label="None" value="" />
                        <option value="front_end">Front End</option>
                        <option value="back_end">Back End</option>
                        <option value="auto_test">Auto Test</option>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={values.agree ? true : false}
                          onChange={handleChange}
                          name="agree"
                        />
                      }
                      label="Remember me"
                    />
                  </Grid>
                  <Box
                    textAlign="center"
                    component="fieldset"
                    m={1}
                    borderColor="transparent"
                  >
                    <Rating
                      name="rating"
                      value={values.rating}
                      onChange={handleChange}
                    />
                  </Box>
                  <Grid className={classes.submit}>
                    <Button
                      color="primary"
                      variant="contained"
                      type="submit"
                      disabled={isSubmitting}
                    >
                      Login
                    </Button>
                    {"    "}
                    <Button
                      variant="contained"
                      type="button"
                      onClick={handleReset}
                    >
                      Clear
                    </Button>
                  </Grid>
                </Form>
              </div>
            </div>
          );
        }}
      </Formik>
    </Grid>
  );
}

export default FormMaterialUI;
