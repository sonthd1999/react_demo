import React, { useState } from "react";
import { Button } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import { Formik } from "formik";
import * as Yup from "yup";

function FormUser() {
  const [initialValue, setInitialValue] = useState({
    email: "",
    password: "",
  });

  const addUser = (email: string, password: string) => {};

  const editUser = (email: string, password: string, id: string) => {};

  const resetUser = () => {
    setInitialValue({
      email: "",
      password: "",
    });
  };
  return (
    <div className="mx-auto w-25 mt-5">
      <h2 className="text-center m-4 text-info">Formik Login</h2>
      <Formik
        enableReinitialize
        initialValues={initialValue}
        onSubmit={(values) => {
          if (!editUser) {
            addUser(values.email, values.password);
          } else {
            editUser(values.email, values.password, user.id);
          }
        }}
        validateOnChange={true}
        validateOnBlur={true}
        validationSchema={Yup.object().shape({
          email: Yup.string().required(),
          password: Yup.string().required(),
        })}
      >
        {(props) => {
          props.submitCount > 0 && (props.validateOnChange = true);
          const {
            values,
            touched,
            errors,
            isSubmitting,
            handleBlur,
            handleChange,
            handleSubmit,
          } = props;

          return (
            <Form onSubmit={handleSubmit}>
              <div className="">
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    id="email"
                    placeholder="Enter your Email"
                    type="email"
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className={
                      errors.email && touched.email
                        ? "text-input error"
                        : "text-input"
                    }
                  />
                  {errors.email && touched.email && (
                    <div className="input-feedback text-change">
                      {errors.email}
                    </div>
                  )}
                </Form.Group>
                <Form.Group controlId="formBasicPassword" className="mt-4">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    id="password"
                    placeholder="Enter your Password"
                    type="password"
                    value={values.password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    className={
                      errors.password && touched.password
                        ? "text-input error"
                        : "text-input"
                    }
                  />
                  {errors.password && touched.password && (
                    <div className="input-feedback text-change">
                      {errors.password}
                    </div>
                  )}
                </Form.Group>
              </div>
              <div className="mt-3 text-center">
                <Button
                  className="m-1 btn-success"
                  type="submit"
                  disabled={isSubmitting}
                >
                  {" "}
                  Login
                  {/* {editUser ? "Update" : "Add"} */}
                </Button>
                <Button
                  type="button"
                  className="m-1 btn-primary"
                  onClick={() => resetUser()}
                >
                  Clear
                </Button>
              </div>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}

export default FormUser;
